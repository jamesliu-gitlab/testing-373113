import requests

# pyspark example from https://gitlab.com/gitlab-org/gitlab/-/issues/373113
savetable \
    .withColumn('ttlinminutes',lit($ttl$)) \
    .write.mode('append').format('orc').insertInto('$customSchema$.all_refreshes')

# "vulnerable" code from this test fixture: https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/blob/45a3269c3f2cc744b857ef8b608da60ebc656870/qa/fixtures/python/pip-flask/tests/e2e_zap.py#L36-37
post = {'id': 2}
auth_header = {"Authorization": auth_token}
proxies = {
    'http': 'http://127.0.0.1:8090',
    'https': 'http://127.0.0.1:8090',
}
fetch_customer_post = requests.post(
    target_url + '/fetch/customer', json=post, proxies=proxies, headers=auth_header, verify=False)